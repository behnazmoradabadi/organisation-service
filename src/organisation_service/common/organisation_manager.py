import json
import logging
import uuid
from typing import Optional

from fastapi import HTTPException
from organisation_service.common.caches.dynamo_cache import DynamoCache, TableItem
from organisation_service.models import Organisation

logger = logging.getLogger(__name__)


class OrganisationManager:
    """Defines a class to manage organisations."""

    organisation_table_name: str = "organisation_table"

    @classmethod
    def save_organisation(cls, organisation: Organisation) -> Organisation:
        """Save an organisation."""
        logger.info(f"Storing organisation: {organisation}")
        if not organisation.id:
            organisation.id = uuid.uuid4()
        DynamoCache.get(table_name=cls.organisation_table_name).put_item(
            Item=json.loads(organisation.json())
        )
        return organisation

    @classmethod
    def get_organisation(self, organisation_id: str) -> Optional[Organisation]:
        """Fetch a profile value."""
        item: TableItem = {"id": organisation_id}
        data = DynamoCache.get(table_name=self.organisation_table_name).get_item(Key=item)
        organisation = data.get("Item")
        if organisation:
            return Organisation.parse_obj(organisation)
        else:
            raise HTTPException(
                status_code=404,
                detail="Organisation not found",
            )

    @classmethod
    def update_organisation(cls, organisation: Organisation) -> Organisation:
        """Update an organisation."""
        logger.info(f"Updating organisation: {organisation}")
        # Make sure the organisation exists
        cls.get_organisation(str(organisation.id))
        DynamoCache.get(table_name=cls.organisation_table_name).put_item(
            Item=json.loads(organisation.json())
        )
        return organisation

    @classmethod
    def patch_organisation(cls, organisation: Organisation) -> Organisation:
        """Patch an organisation."""
        logger.info(f"Patching organisation: {organisation}")
        # Make sure the organisation exists
        original_organisation = cls.get_organisation(str(organisation.id))
        original_organisation_dict: dict = json.loads(original_organisation.json())
        logger.info(f"current organisation data: {original_organisation_dict}")
        patch_organisation_dict: dict = json.loads(organisation.json())
        # filter fields with value
        patch_organisation_dict = {
            key: value for key, value in patch_organisation_dict.items() if value
        }
        logger.info(f"patch organisation data: {original_organisation_dict}")
        original_organisation_dict.update(patch_organisation_dict)
        DynamoCache.get(table_name=cls.organisation_table_name).put_item(
            Item=original_organisation_dict
        )
        return Organisation.parse_obj(original_organisation_dict)
