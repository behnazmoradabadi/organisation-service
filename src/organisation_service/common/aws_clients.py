import os

import boto3
from mypy_boto3_dynamodb import DynamoDBServiceResource

ENDPOINT_URL = os.environ.get("ENDPOINT_URL")


def dynamodb_resource() -> DynamoDBServiceResource:
    """Return a dynamodb resource.

    Returns:
        DynamoDBServiceResource: the dynamodb resource
    """
    return boto3.resource("dynamodb", endpoint_url=ENDPOINT_URL)
