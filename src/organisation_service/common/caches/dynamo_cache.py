import time
from decimal import Decimal
from typing import Any, Mapping, Sequence, Set, Union

from mypy_boto3_dynamodb.service_resource import Table
from organisation_service.common.aws_clients import dynamodb_resource
from organisation_service.common.caches.base_cache import BaseCache, CacheEntry

# Copied verbatim from mypy_boto3_dynamodb/service_resource.pyi
TableItem = Mapping[
    str,
    Union[
        bytes,
        bytearray,
        str,
        int,
        Decimal,
        bool,
        Set[int],
        Set[Decimal],
        Set[str],
        Set[bytes],
        Set[bytearray],
        Sequence[Any],
        Mapping[str, Any],
        None,
    ],
]


class DynamoCache(BaseCache[Table]):
    """Defines a class to cache dynamo table connections."""

    TTL: int = 300

    @classmethod
    def create(cls, **kwargs: str) -> CacheEntry[Table]:
        """Create a DynamoConnEntry instance.

        Args:
            table_name: The name of dynamodb table to be cache.
        """
        table = dynamodb_resource().Table(kwargs["table_name"])
        return CacheEntry[Table](expires=time.time() + cls.TTL, value=table)
