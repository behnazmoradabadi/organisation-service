import time
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Generic, TypeVar

T = TypeVar("T")


@dataclass
class CacheEntry(Generic[T]):
    """Capture cache entries."""

    expires: float
    value: T


class BaseCache(ABC, Generic[T]):
    """Defines a class to cache an entity."""

    _cache: dict[int, CacheEntry[T]] = {}
    TTl: int = 300

    def __init_subclass__(cls, **kwargs) -> None:
        """Create class variables for each subclass.

        This obviates the need to have each subclass define them manually.
        """
        super().__init_subclass__(**kwargs)
        cls._cache = {}

    @classmethod
    def purge(cls) -> None:
        """Purges cache."""
        cls._cache = {}

    @classmethod
    def get(cls, **kwargs) -> T:
        """Returns a cached entity."""
        entry_hash = hash(tuple(kwargs.values()))
        cached_entry = cls._cache.get(entry_hash)

        # Determine if the cache is expired
        if cached_entry and time.time() < cached_entry.expires:
            return cached_entry.value

        # Create a new value and cache it so it can be used for future requests
        new_entry = cls.create(**kwargs)
        cls._cache[entry_hash] = new_entry
        return new_entry.value

    @classmethod
    @abstractmethod
    def create(cls, **kwargs) -> CacheEntry[T]:
        """Creates an entity to be cached."""
        raise NotImplementedError()
