import logging

from fastapi import APIRouter, Request
from organisation_service.common.organisation_manager import OrganisationManager
from organisation_service.models import Organisation, OrganisationPatchRequest

logger = logging.getLogger(__name__)
router = APIRouter()


@router.post(
    "/v1/organisations",
    response_model=Organisation,
    response_model_exclude_unset=True,
)
def post_organisation(organisation: Organisation, request: Request) -> Organisation:
    """Receives POST /v1/organisations request."""
    organisation_response = OrganisationManager.save_organisation(organisation)
    return organisation_response


@router.get(
    "/v1/organisations/{organisation_id}",
    response_model=Organisation,
    response_model_exclude_unset=True,
)
def get_organisation(organisation_id: str, request: Request) -> Organisation:
    """Receives POST /v1/organisations request."""
    organisation = OrganisationManager.get_organisation(organisation_id)
    return organisation


@router.put(
    "/v1/organisations/{organisation_id}",
    response_model=Organisation,
    response_model_exclude_unset=True,
)
def put_organisation(
    organisation_id: str, organisation: Organisation, request: Request
) -> Organisation:
    """Receives PUT /v1/organisations request."""
    organisation.id = organisation_id
    organisation_response = OrganisationManager.update_organisation(organisation)
    return organisation_response


@router.patch(
    "/v1/organisations/{organisation_id}",
    response_model=Organisation,
    response_model_exclude_unset=True,
)
def patch_organisation(
    organisation_id: str, organisation: OrganisationPatchRequest, request: Request
) -> Organisation:
    """Receives PATCH /v1/organisations request."""
    organisation.id = organisation_id
    organisation_response = OrganisationManager.patch_organisation(organisation)
    return organisation_response
