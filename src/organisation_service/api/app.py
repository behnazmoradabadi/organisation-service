import logging
import os
import secrets

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from mangum import Mangum
from organisation_service.api.controllers.organisations import router

logger = logging.getLogger(__name__)


security = HTTPBasic()


def authorize_user(credentials: HTTPBasicCredentials = Depends(security)):  # noqa: B008
    """Authorize user."""
    current_username_bytes = credentials.username.encode("utf8")
    # TODO: should be replaced with a better way
    correct_username_bytes = os.getenv("USERNAME")
    correct_password_bytes = os.getenv("PASSWORD")
    is_correct_username = secrets.compare_digest(current_username_bytes, correct_username_bytes)
    current_password_bytes = credentials.password.encode("utf8")
    is_correct_password = secrets.compare_digest(current_password_bytes, correct_password_bytes)
    if not (is_correct_username and is_correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect user or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username


app = FastAPI(
    title="Organisation Service API",
    version="0.1.0",
    root_path=os.getenv("API_ROOT_PATH", "/"),
    root_path_in_servers=True,
    dependencies=[Depends(authorize_user)],
)
app.include_router(router)

handler = Mangum(app, lifespan="off")
