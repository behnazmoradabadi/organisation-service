"""Top level package for the Organisation service."""
import logging
from typing import Literal


class LogSettings:
    """Take log level from environment variable."""

    LOG_LEVEL: Literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"] = "INFO"


log_settings = LogSettings()


logging.getLogger().setLevel(log_settings.LOG_LEVEL)
