from __future__ import annotations

import re
import uuid
from typing import Optional

from pydantic import BaseModel, validator


class ProjectBaseModel(BaseModel):
    """Define a ProjectBaseModel for the system."""

    class Config:
        """Apply some configs to BaseModel."""

        extra = "allow"
        validate_assignment = True


class Organisation(ProjectBaseModel):
    """Defines a Organisation structure and types."""

    id: Optional[uuid.UUID]
    name: str
    contact_email: str
    address: str

    @validator("contact_email")
    def validate_contact_email(cls, value):
        """Validate email."""
        regex = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b"
        if not (re.fullmatch(regex, value)):
            raise ValueError("Email is not valid")
        return value


class OrganisationPatchRequest(Organisation):
    """Defines a Organisation Patch structure and types."""

    name: Optional[str]
    contact_email: Optional[str]
    address: Optional[str]
