from unittest.mock import patch

from organisation_service.api.controllers.organisations import (
    get_organisation,
    patch_organisation,
    post_organisation,
    put_organisation,
)
from organisation_service.models import Organisation


@patch("organisation_service.api.controllers.organisations.OrganisationManager")
def test_post_organisation_api(
    mock_organisation_manager, organisation: Organisation, mock_request
) -> None:
    mock_request.scope = {"path": "/v1/organisations"}
    mock_organisation_manager.save_organisation.return_value = organisation
    response = post_organisation(organisation, mock_request)
    mock_organisation_manager.save_organisation.assert_called_once_with(organisation)
    assert isinstance(response, Organisation)


@patch("organisation_service.api.controllers.organisations.OrganisationManager")
def test_get_organisation_api(
    mock_organisation_manager, organisation: Organisation, mock_request
) -> None:
    mock_request.scope = {"path": "/v1/organisations"}
    mock_organisation_manager.get_organisation.return_value = organisation
    response = get_organisation(organisation.id, mock_request)
    mock_organisation_manager.get_organisation.assert_called_once_with(organisation.id)
    assert isinstance(response, Organisation)


@patch("organisation_service.api.controllers.organisations.OrganisationManager")
def test_put_organisation_api(
    mock_organisation_manager, organisation: Organisation, mock_request
) -> None:
    mock_request.scope = {"path": "/v1/organisations"}
    mock_organisation_manager.update_organisation.return_value = organisation
    response = put_organisation(organisation.id, organisation, mock_request)
    mock_organisation_manager.update_organisation.assert_called_once_with(organisation)
    assert isinstance(response, Organisation)


@patch("organisation_service.api.controllers.organisations.OrganisationManager")
def test_patch_organisation_api(
    mock_organisation_manager, organisation: Organisation, mock_request
) -> None:
    mock_request.scope = {"path": "/v1/organisations"}
    mock_organisation_manager.patch_organisation.return_value = organisation
    response = patch_organisation(organisation.id, organisation, mock_request)
    mock_organisation_manager.patch_organisation.assert_called_once_with(organisation)
    assert isinstance(response, Organisation)
