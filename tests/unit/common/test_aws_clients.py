from organisation_service.common.aws_clients import dynamodb_resource


def test_dynamodb_resource() -> None:
    resource = dynamodb_resource()
    assert str(type(resource)) == "<class 'boto3.resources.factory.dynamodb.ServiceResource'>"
