from unittest.mock import Mock, patch

from organisation_service.common.organisation_manager import OrganisationManager
from organisation_service.models import Organisation


@patch("organisation_service.common.organisation_manager.DynamoCache")
def test_saveorganisation(
    mock_dynamodb_cache: Mock,
    organisation: Organisation,
) -> None:
    OrganisationManager.save_organisation(organisation)
