from unittest.mock import Mock, patch

from organisation_service.common.caches.base_cache import CacheEntry
from organisation_service.common.caches.dynamo_cache import DynamoCache, Table


def test_dynamo_cache_entry() -> None:
    dynamo_cache_entry = CacheEntry[Table](expires=100.0, value=Mock())

    assert dynamo_cache_entry.value is not None
    assert dynamo_cache_entry.expires == 100.0


@patch("organisation_service.common.caches.dynamo_cache.dynamodb_resource")
def test_dynamo_cache_create(mock_dynamo_resource: Mock) -> None:
    entry = DynamoCache.create(table_name="table_name")
    mock_dynamo_resource.assert_called()
    assert isinstance(entry, CacheEntry)


@patch("organisation_service.common.caches.dynamo_cache.dynamodb_resource")
def test_dynamo_cache_get(mock_dynamo_resource: Mock) -> None:
    mock_dynamo_resource().Table.return_value = Mock()
    DynamoCache.purge()
    DynamoCache.get(table_name="table_name")
    DynamoCache.get(table_name="table_name")
    DynamoCache.get(table_name="table_name1")
    assert len(DynamoCache._cache.keys()) == 2  # type: ignore
