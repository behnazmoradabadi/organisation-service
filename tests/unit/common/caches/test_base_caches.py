import time
from typing import Optional
from unittest import mock

import pytest
from organisation_service.common.caches.base_cache import BaseCache, CacheEntry


def test_cache_entry() -> None:
    cache_entry = CacheEntry[int](expires=100.0, value=1)

    assert cache_entry.value == 1
    assert cache_entry.expires == 100.0


def test_cache_purge() -> None:
    ttl = 60
    cache_entry = CacheEntry[int](expires=time.time() + ttl, value=1)

    class IntCache(BaseCache[int]):
        @classmethod
        def create(cls, **kwargs) -> CacheEntry[int]:
            return cache_entry

    int_cache = IntCache()
    cache_value = int_cache.get(key="whatever")
    assert cache_value == cache_entry.value

    int_cache.purge()
    assert int_cache._cache == {}


@mock.patch("organisation_service.common.caches.base_cache.BaseCache.create")
@pytest.mark.parametrize(
    "mock_entry, time_adjustment", [(None, 0), (mock.Mock(), 3600), (mock.Mock(), -3600)]
)
def test_cache_get(
    mock_create: mock.Mock, mock_entry: Optional[mock.Mock], time_adjustment: int
) -> None:
    test_kwargs = {"arg1": "value1", "arg2": "value2"}
    target_hash = hash(tuple(test_kwargs.values()))

    # Cache should be clean in every test
    BaseCache.purge()

    if mock_entry is not None:
        # Create an value in the cache
        BaseCache._cache[target_hash] = CacheEntry[mock.Mock](  # type: ignore
            time.time() + time_adjustment, mock_entry
        )

    retrieved_entry: CacheEntry[Optional[mock.Mock]] = BaseCache.get(**test_kwargs)
    stored_entry: Optional[mock.Mock] = BaseCache._cache[target_hash].value  # type: ignore

    assert retrieved_entry == stored_entry

    # In the cases where the cache is empty or expired, the cache should be filled and should
    # be equal to what we retrieved, which is the result of calling .create()
    if mock_entry is None or time_adjustment < 0:
        mock_create.assert_called()
        assert retrieved_entry == mock_create().value
