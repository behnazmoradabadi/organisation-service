import os
import uuid

import pytest
from organisation_service.models import Organisation


@pytest.fixture(autouse=True, scope="session")
def setup_env():
    os.environ["AWS_DEFAULT_REGION"] = "us-east-1"
    os.environ["AWS_ACCESS_KEY_ID"] = "000000000000"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "secret"  # noqa: S105


class MockRequest:
    def __init__(self) -> None:
        self.method = "GET"


@pytest.fixture
def mock_request() -> MockRequest:
    return MockRequest()


@pytest.fixture
def organisation() -> Organisation:
    return Organisation(
        id=uuid.uuid4(), name="name", contact_email="behnaz@gmail.com", address="street1"
    )
