.ONESHELL:
.SILENT:

deploy-local-stack:
	echo "Cleaning for stack"
	rm -rf cdk.out/
	docker-compose down --volumes --remove-orphans
	docker-compose up --build -d localstack
	export AWS_SECRET_ACCESS_KEY=secret
	cdklocal bootstrap aws://000000000000/us-east-1 --verbose
	cdklocal deploy organisation-service-stack --require-approval=never
