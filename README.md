# Organisation Service

## Initial Setup

### Poetry

We use Poetry as the Python package manager.
- Go to the source folder and activate poetry managed virtual environment
    ```zsh
    poetry env use python
    ```
- To verify a poetry managed virtual environment is active run:
    ```zsh
    poetry env list
    ```
- Install project and its dependencies:
    ```zsh
    poetry install
    ```
### pre-commit

We are using an extensive set of pre-commit hooks
to keep our code sane
and prevent any discussions over style.
The pre-commit hooks needs to be installed first though:

```zsh
pre-commit install
```

### CDK

We use CDK for our deployment.
It requires the following software packages:

* AWS CLI v2 ([Installation Instructions](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html))
* AWS CDK (`npm install -g aws-cdk`)

#### Local setup

To run cdk locally please follow these steps:

- Install `cdklocal` that is compatible to the used `aws-cdk-lib` version (`npm install -g aws-cdk-local`)
- Run `make deploy-local-stack` to run localstack, synth the infra, bootstrap and deploy the app locally.

- To test the API you need to set these two environment variables: `USERNAME` and `PASSWORD`


#### Pytest

When running Pytest `poetry run pytest` you can add the option `--cov` to generate the coverage table and if you want to go more in dept into the testing you can even run `poetry run pytest --cov --cov-report html` which will create a report in HTML (stored in `/htmlcov`) which you see by accessing the `index.html` file inside via the browser.


#### Swagger 
The `openapi.json` has been exposed under `<domain>/openapi.json`.

