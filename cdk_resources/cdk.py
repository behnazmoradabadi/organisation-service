#!/usr/bin/env python3

import os
from dataclasses import dataclass
from typing import Optional

import aws_cdk as cdk

from cdk_resources.stack import OrganisationServiceStack

STACK_BUILD_TARGET = os.getenv("STACK_BUILD_TARGET", "local")
app = cdk.App()


@dataclass
class EnvConfig:
    """Base class for environment setup for the stacks."""

    aws_account_id: str
    aws_region: str
    endpoint_url: Optional[str] = None


def create_stack(config: EnvConfig) -> None:
    """Create organisation service stack."""
    OrganisationServiceStack(
        scope=app,
        construct_id="organisation-service-stack",
        endpoint_url=config.endpoint_url,
    )


if STACK_BUILD_TARGET == "local":
    create_stack(
        EnvConfig(
            aws_account_id="000000000000",
            aws_region="us-east-1",
            endpoint_url="http://localhost.localstack.cloud:4566/",
        )
    )

if STACK_BUILD_TARGET == "prod":
    create_stack(
        EnvConfig(
            aws_account_id="",
            aws_region="us-east-1",
        )
    )

app.synth()
