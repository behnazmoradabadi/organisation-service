from aws_cdk import BundlingOptions, Duration, RemovalPolicy, Stack
from aws_cdk import aws_apigateway as apigw
from aws_cdk import aws_dynamodb as dynamodb
from aws_cdk import aws_lambda as _lambda
from aws_cdk.aws_dynamodb import Table
from aws_cdk.aws_lambda import Runtime
from aws_cdk.aws_logs import LogGroup, RetentionDays
from constructs import Construct


class OrganisationServiceStack(Stack):
    """Initialize an Organisation Service CDK Stack.

    Args:
        scope: the parent CDK application instance
        construct_id: the CDK resource name for the new stack
        endpoint_url: a dict of override endpoint URL's for various AWS services
    """

    def __init__(
        self,
        scope: Construct,
        construct_id: str,
        endpoint_url: str = None,
        **kwargs,
    ) -> None:
        """Generate CDK resources."""
        super().__init__(scope, construct_id, **kwargs)

        self.lambda_runtime: Runtime = _lambda.Runtime.PYTHON_3_9
        self.endpoint_url = endpoint_url
        self.env_vars: dict[str, str] = {}
        if self.endpoint_url:
            self.env_vars["ENDPOINT_URL"] = self.endpoint_url

        self.log_group = LogGroup(
            self,
            "Organisation_Service",
            removal_policy=RemovalPolicy.DESTROY,
            retention=RetentionDays.THREE_MONTHS,
        )

        self.construct_id = construct_id
        self.organisation_table: Table = self._create_organisation_tables()
        self.api_lambda = self._setup_rest_api_lambda()
        self.rest_api = self._setup_rest_api()
        self._apply_grants()

    def _apply_grants(self):
        """Applies all the grants in one place!

        At the end of the deployment
        generation process. This avoids circular dependencies between the
        functions that create the resources.
        """
        self.organisation_table.grant_read_write_data(self.api_lambda.role)

    def _setup_rest_api(self) -> apigw.LambdaRestApi:
        api = apigw.LambdaRestApi(
            self,
            "organisation_api",
            handler=self.api_lambda,
            rest_api_name="Organisation Service API",
            description="This service provides the Organisation Service API.",
            proxy=False,
            deploy_options=apigw.StageOptions(
                logging_level=apigw.MethodLoggingLevel.INFO,
                access_log_destination=apigw.LogGroupLogDestination(self.log_group),
                data_trace_enabled=True,
                metrics_enabled=True,
                stage_name="prod",
            ),
        )

        api.root.add_method(
            "GET",
        )
        api.root.add_resource("openapi.json").add_method(
            "GET",
        )
        api.root.add_resource("docs").add_method(
            "GET",
        )
        organisation = api.root.add_resource("v1").add_resource("organisations")
        organisation.add_method("POST")
        update_resource = organisation.add_resource("{organisation_id}")
        update_resource.add_method("GET")
        update_resource.add_method("PUT")
        update_resource.add_method("PATCH")

        return api

    def _setup_rest_api_lambda(self) -> _lambda.Function:
        return self._create_lambda_function(
            "organisation_service.api.app.handler",
        )

    def _create_lambda_function(
        self,
        name: str,
        extra_env_vars=None,
        timeout_seconds=30,
        memory_size_mb=256,
        log_retention: RetentionDays = RetentionDays.THREE_MONTHS,
    ) -> _lambda.Function:
        bundled_code = self._build_code_bundle()

        print(f"Creating lambda {name}")
        return _lambda.Function(
            self,
            name,
            runtime=self.lambda_runtime,
            code=bundled_code,
            handler=name,
            environment=self.env_vars,
            timeout=Duration.seconds(timeout_seconds),
            memory_size=memory_size_mb,
            log_retention=log_retention,
        )

    def _build_code_bundle(self) -> _lambda.Code:
        # The bundling is performed by processing /asset-input/ and creating/coping
        # the necessary lambda contents at /asset-output/ inside a docker container
        bundle_script = [
            "pip install --no-cache --upgrade pip poetry",
            "poetry build --format wheel",
            "mkdir /tmp/build",
            "pip install --target /tmp/build ./dist/*.whl",
            "cd /tmp/build/",
            "rm -rf *.dist-info",  # a bit smaller build
            "zip -9 -r /asset-output/code.zip ./*",  # highest compression
            "rm -rf /asset-input/dist/",  # clean up to avoid polluting dev workspace
        ]

        # Bundle the code for the lambda
        return _lambda.Code.from_asset(
            # Set the path in which the code is bundled
            path=".",
            # Set various bundling options
            bundling=BundlingOptions(
                # Select the bundling image which corresponds to the runtime
                image=self.lambda_runtime.bundling_image,
                user="root:root",
                # Execute the bundle script in bash
                command=["bash", "-c", " && ".join(bundle_script)],
            ),
        )

    def _create_organisation_tables(self):
        """Setup a DynamoDB table for storing many to organisations."""
        organisation_table = dynamodb.Table(
            self,
            id="organisation_table",
            table_name="organisation_table",
            partition_key=dynamodb.Attribute(name="id", type=dynamodb.AttributeType.STRING),
            billing_mode=dynamodb.BillingMode.PAY_PER_REQUEST,
        )
        return organisation_table
